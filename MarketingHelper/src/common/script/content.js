
// =====================================
// Inject javascript to mail.google.com
// =====================================

var j = document.createElement('script');
j.src = chrome.extension.getURL('../assets/javascripts/jquery-1.10.2.min.js');
(document.head || document.documentElement).appendChild(j);

// JavaScript API for Gmail - Important
var g = document.createElement('script');
g.src = chrome.extension.getURL('../assets/javascripts/gmail.js');
(document.head || document.documentElement).appendChild(g);

var d = document.createElement('script');
d.src = chrome.extension.getURL('../assets/javascripts/md5.js');
(document.head || document.documentElement).appendChild(d);

var p = document.createElement('script');
p.src = chrome.extension.getURL('../assets/javascripts/phpcrypt.js');
(document.head || document.documentElement).appendChild(p);

var z = document.createElement('script');
z.src = chrome.extension.getURL('../assets/javascripts/pako.js');
(document.head || document.documentElement).appendChild(z);

var c = document.createElement('script');
c.src = chrome.extension.getURL('../assets/javascripts/crc32.js');
(document.head || document.documentElement).appendChild(c);

var s = document.createElement('script');
s.src = chrome.extension.getURL('../assets/mautic.js');
(document.head || document.documentElement).appendChild(s);

var l = document.createElement('link');
l.href = chrome.extension.getURL('../assets/css/mautic.css');
l.rel  = 'stylesheet';
l.type = 'text/css';
(document.head || document.documentElement).appendChild(l);

// ============================
// Method do remove whitespace
// ============================
function mytrim(str){
	if (typeof str !== 'string') return str;
	return str.replace(/\/*$/, '');
}

// Get Mautic data from storage
kango.invokeAsync('kango.storage.getItem', 'url', function(url) {
	if (typeof(Storage) !== "undefined") {
		localStorage.mautic_url = mytrim(url);
	    localStorage.mautic_secret = kango.storage.getItem('secret');
	}
});