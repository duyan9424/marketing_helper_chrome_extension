// ============================
// Method do after Kango ready
// ============================
KangoAPI.onReady(function() {
    
    // Bind an event handler to the "click" JavaScript event
    $('#goto').click(function(){
        kango.ui.optionsPage.open();
        KangoAPI.closeWindow();
    });

});