# Marketing Helper
   * Marketing Helper is a Chrome Extension for Gmail
   * This extension uses the [Kango framework](http://kangoextensions.com) - cross-browser extension framework

## How to install a Chrome extension without using the Chrome Web Store
   - from a .CRX file in `MarketingHelper/output/marketinghelper_0.9.0.crx` project directory. [Download Here](https://bitbucket.org/duyan9424/marketing_helper_chrome_extension/raw/d503b3000460ac9579851691537a38bd9f99bf7a/MarketingHelper/output/marketinghelper_0.9.0.crx)
   - go to chrome://extensions (or find it in your menu in more tools > extensions)
   - then activate the developer mode on the top right of the page.
   - then drag and drop the .CRX file on the extension page.

## How to Load unpacked extension Developer mode for testing on Chrome.

   - Click Preferences > Go to More tools > then Extensions > check Develper Mode.
   - Load unpacked extension…
   - Select extension directory: `MarketingHelper\output\chrome`

## Preparation of environment to work with Kango. [Window]
Before you begin working with Kango you should make a few steps:

1. Install Python 2.7 (http://www.python.org/download/) 
2. Download Download Kango v1.8.0 [Here](http://kangoextensions.com/kango/kango-framework-latest.zip) and extract the archive with framework to project directory.

we need to add Python to the environment variables. [Here](https://www.londonappdeveloper.com/setting-up-your-windows-10-system-for-python-development-pydev-eclipse-python/)

## Get extension project you can start to edit
   - Source code should be placed in `MarketingHelper/src/common/` project directory.
   - If SourceCode changed. Go to chrome://extensions/ > Reload (Ctrl+R)
## Building extension
   - change direcoty to `MarketingHelper` and run command in Window.
  ```
  MarketingHelper>build.cmd
  # output
  [   INFO] Contact extensions@kangoextensions.com to enable IE support
  [   INFO] Running Kango v1.8.0
  [   INFO] Building chrome extension...
  [   INFO] Building firefox extension...
  [   INFO] Building safari extension...
  ```
   - And you see result in `MarketingHelper/output/` project directory.
  ```
  marketinghelper_0.9.0.crx
  marketinghelper_0.9.0.xpi
  marketinghelper_0.9.0_chrome_webstore
  ```

## SourceCode Path Listing:  
```
MarketingHelper/
└───src
    ├───common
    │   ├───assets
    │   │   ├───css
    │   │   ├───fonts
    │   │   ├───icons
    │   │   ├───javascripts
    │   │   └───vendor
    │   ├───locales
    │   ├───pages
    │   └───script
```